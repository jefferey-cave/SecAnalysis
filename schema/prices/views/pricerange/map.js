function(doc){
	var cik = doc.cik;
	var value = {};

	var relevant = ['symbol','close','open','high','low'].indexOf(doc.fact) >= 0;
	if(!relevant) return;

	value[doc.fact] = doc.val;

	if(doc.fact !== 'symbol'){
		var end = doc.end.split('-');end[1]--;
		value.end = Date.UTC(end[0],end[1],end[2]);
		value.start = value.end;
		value.symbol = doc.symbol;
	}
	//value.url = 'https://query1.finance.yahoo.com/v7/finance/download/'+value.ticker+'?period1=1554336124&period2=1556928124&interval=1d&events=history&crumb=QOpsW4BfabA';
	//value.url = "https://api.tiingo.com/tiingo/daily/{ticker}/prices?startDate={start}&endDate={end}"
	//	.replace(/{ticker}/g,value.ticker)
	//	.replace(/{start}/g,value.start)
	//	.replace(/{end}/g,value.end)
	//	;
  
	emit(cik,value);
}
