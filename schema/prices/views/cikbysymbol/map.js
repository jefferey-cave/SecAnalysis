/**
  * cikbysymbol
  *
  */
 function(doc){
	if(!doc.fact) return;
	if(doc.fact !== 'symbol') return;

	var cik = doc.cik;
	var symbol = doc.val.toUpperCase();

	var date = doc.end;
	var key = date.split('-');
	for(var k in key){
		key[k] = +key[k];
	}
	key.unshift(symbol);
	key.push(cik);
	emit([].concat(key),1);

	key.shift();
	key.unshift(key.pop());
	key.push(symbol);
	emit(key,1);
}
