# SEC filing scraper

[![pipeline status](https://gitlab.com/securities-tools/SecAnalysis/badges/master/pipeline.svg)](https://gitlab.com/securities-tools/SecAnalysis/-/commits/master)

The SEC offers a wealth of data on their website, but how do we make sense of it. This is a set of scripts and database configurations that allow us to get local copies of the data in a shared manner.


## Installation

### Dependencies

1. Cloudflare R2 account
2. Tingo key

## Links

* https://www.sec.gov/edgar/searchedgar/accessing-edgar-data.htm
* https://github.com/datasets/edgar
* https://datahub.io/dataset/corpwatch
* ftp://ftp.sec.gov/edgar/
** note that FTP service has been discontinued


