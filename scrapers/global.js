import fs from 'fs';
import PouchDB from 'pouchdb';
import path from "path";
import {
	S3Client,
	ListBucketsCommand,
	ListObjectsV2Command,
	GetObjectCommand,
	PutObjectCommand,
	DeleteObjectCommand,
	//ObjectMetadata
} from "@aws-sdk/client-s3";
import cliProgress from 'cli-progress';

export {
	sec,
	settings,
	multibar as progress,
	halt,
}

console.log(`Node Version: ${process.version}`);

let settings = process.argv.slice();
settings.shift();
settings.shift();
settings = settings.join(' ').trim();
if(!settings){
	settings = process.cwd();
	settings = `${settings}/scrapers/secrets.json`;
}
settings = path.normalize(settings);
settings = path.resolve(settings);
console.log("Loading: " + settings);
settings = JSON.parse(fs.readFileSync(settings, 'utf8'));



// create new container
const multibar = new cliProgress.MultiBar({
    clearOnComplete: false,
    hideCursor: false,
	noTTYOutput: true,
	notTTYSchedule: 60000,
    format: '|{bar}| {label} | {value}/{total} | {duration_formatted} |',
}, cliProgress.Presets.shades_grey);
let membar = multibar.create(512, 0,{
    format: '|{bar}| {label} | {value}/{total} |',
});
setInterval(()=>{
	let used = process.memoryUsage().heapUsed
	used /= (1024**2);
	used = Math.floor(used);
	membar.update(used,{label:'RAM (MB)'});
},512);


let halt = false;
let timeout = settings.timeout ?? 1000 * 60 * 60 * 24;
setTimeout(()=>{
	halt = true;
	multibar.log('---------Timeout: Terminating Processing---------\n');
	setTimeout(()=>{
		multibar.log('---------Timeout: Hard terminate---------\n');
		process.exit(3);
	},1000*60*10);
},timeout);

let opts = {};
if(settings.storage.ibmgrant && settings.storage.ibmgrant.username && settings.storage.ibmgrant.password){
	opts.auth = {
		username: settings.storage.ibmgrant.username,
		password: settings.storage.ibmgrant.password,
	};
}




let S3 = null;
function getS3Client(settings){
	if (S3) return S3;

	let ACCOUNT_ID = settings.storage.r2Facts.AcctId;
	let ACCESS_KEY_ID = settings.storage.r2Facts.AccessKey;
	let SECRET_ACCESS_KEY = settings.storage.r2Facts.SecretKey;

	//console.log(`ACCOUNT_ID.........: ${ACCOUNT_ID} `);
	//console.log(`ACCESS_KEY_ID......: ${ACCESS_KEY_ID} `);
	//console.log(`SECRET_ACCESS_KEY..: ${SECRET_ACCESS_KEY} `);

	S3 = new S3Client({
		region: "auto",
		endpoint: `https://${ACCOUNT_ID}.r2.cloudflarestorage.com`,
		credentials: {
			accessKeyId: ACCESS_KEY_ID,
			secretAccessKey: SECRET_ACCESS_KEY,
		},
	});

	return S3;
}



let sec = getS3Client(settings);
sec.send(new GetObjectCommand({Bucket: 'facts', Key: 'views/facts'}))
	.then((result)=>{
		let r = result.Body.read();
		console.log(r);
		if(r){
			console.log('Test Connection successful');
		}
	}).catch(e=>{
		console.error("failed to instantiate database connection");
		//console.error(e);
		//process.exit(2);
	})
	;
