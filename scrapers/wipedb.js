

import fetch from 'node-fetch';
import * as Streams from 'stream';


import * as global from './global.js';
import {
	GetObjectCommand,
	HeadObjectCommand,
	DeleteObjectCommand,
	ListObjectsV2Command
} from "@aws-sdk/client-s3";



async function DeleteAllData(){
	let objs = ['init value'];
	while(objs.length > 0){
		objs = await global.sec.send(new ListObjectsV2Command({Bucket: 'facts', Prefix: "company",}));
		objs = objs.Contents ?? [];
		console.log(`Found ${objs.length} objects`);
		let dels = [];
		for(let obj of objs){
			let del =  global.sec.send(new DeleteObjectCommand({Bucket: 'facts', Key: obj.Key}));
			dels.push(del);
		}
		dels = await Promise.all(dels);
		console.log(`Removed ${dels.length} objects`);
	}
	return;

}

DeleteAllData();
