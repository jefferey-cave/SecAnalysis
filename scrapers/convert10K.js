const stringify = require('json-stable-stringify');
const utils = require('./lib/utils.js');
const global = require('./global.js');
const db = global.db;

'use strict';

const isINCEXP = [
	//"EntityRegistrantName", //: 'AMAZON COM INC',
	//"CurrentFiscalYearEndDate", //: '--12-31',
	//"EntityCentralIndexKey", // : '0001018724',
	//"EntityFilerCategory", // : 'Large Accelerated Filer',
	//"TradingSymbol", // : 'AMZN',
	//"DocumentPeriodEndDate", // : '2015-12-31',
	//"DocumentFiscalYearFocus", // : '2015',
	//"DocumentFiscalPeriodFocus", // : 'FY',
	//"DocumentFiscalYearFocusContext", // : 'FD2015Q4YTD',
	//"DocumentFiscalPeriodFocusContext", // : 'FD2015Q4YTD',
	//"DocumentType", // : '10-K',
	//"BalanceSheetDate", // : '2015-12-31',
	//"IncomeStatementPeriodYTD", // : '2015-01-01',
	//"ContextForInstants", // : 'FI2015Q4',
	//"ContextForDurations", // : 'FD2015Q4YTD',
	//"Assets", // : 65444000000,
	//"CurrentAssets", // : 36474000000,
	//"NoncurrentAssets", // : 28970000000,
	//"LiabilitiesAndEquity", // : 65444000000,
	//"Liabilities", // : 52060000000,
	//"CurrentLiabilities", // : 33899000000,
	//"NoncurrentLiabilities", // : 18161000000,
	//"CommitmentsAndContingencies", // : 0,
	//"TemporaryEquity", // : 0,
	//"Equity", // : 13384000000,
	//"EquityAttributableToNoncontrollingInterest", // : 0,
	//"EquityAttributableToParent", // : 13384000000,
	"Revenues", // : 107006000000,
	"CostOfRevenue", // : 71651000000,
	"GrossProfit", // : 35355000000,
	"OperatingExpenses", // : 33122000000,
	"CostsAndExpenses", // : 104773000000,
	"OtherOperatingIncome", // : 0,
	"OperatingIncomeLoss", // : 2233000000,
	"NonoperatingIncomeLoss", // : -665000000,
	"InterestAndDebtExpense", // : 0,
	"IncomeBeforeEquityMethodInvestments", // : 1568000000,
	"IncomeFromEquityMethodInvestments", // : -22000000,
	"IncomeFromContinuingOperationsBeforeTax", // : 1568000000,
	"IncomeTaxExpenseBenefit", // : 950000000,
	"IncomeFromContinuingOperationsAfterTax", // : 596000000,
	"IncomeFromDiscontinuedOperations", // : 0,
	"ExtraordaryItemsGainLoss", // : 0,
	"NetIncomeLoss", // : 596000000,
	"NetIncomeAvailableToCommonStockholdersBasic", // : 596000000,
	"PreferredStockDividendsAndOtherAdjustments", // : 0,
	"NetIncomeAttributableToNoncontrollingInterest", // : 0,
	"NetIncomeAttributableToParent", // : 596000000,
	"OtherComprehensiveIncome", // : -212000000,
	"ComprehensiveIncome", // : 384000000,
	"ComprehensiveIncomeAttributableToParent", // : 384000000,
	"ComprehensiveIncomeAttributableToNoncontrollingInterest", // : 0,
	"NonoperatingIncomeLossPlusInterestAndDebtExpense", // : -665000000,
	"NonoperatingIncomePlusInterestAndDebtExpensePlusIncomeFromEquityMethodInvestments", // : -665000000,
	"NetCashFlow", // : 1333000000,
	"NetCashFlowsOperating", // : 11920000000,
	"NetCashFlowsInvesting", // : -6450000000,
	"NetCashFlowsFinancing", // : -3763000000,
	"NetCashFlowsOperatingContinuing", // : 11920000000,
	"NetCashFlowsInvestingContinuing", // : -6450000000,
	"NetCashFlowsFinancingContinuing", // : -3763000000,
	"NetCashFlowsOperatingDiscontinued", // : 0,
	"NetCashFlowsInvestingDiscontinued", // : 0,
	"NetCashFlowsFinancingDiscontinued", // : 0,
	"NetCashFlowsDiscontinued", // : 0,
	"ExchangeGainsLosses", // : -374000000,
	"NetCashFlowsContinuing", // : 1707000000,

	// Possibly need to be independantly calculated
	"SGR", // : 0.046606193306224585,
	"ROA", // : 0.009107022798117474,
	"ROE", // : 0.04453078302450687,
	"ROS", // : 0.0055697811337682
];

async function CreateQ4(cik,year,duration){
	cik = +cik;
	year = +year;
	duration.DocumentFiscalPeriodFocus = 'Q4';

	console.log(JSON.stringify([cik,year]));
	let rs = await db.query('filings/financials',{
		include_docs:true,
		inclusive_end:true,
		reduce:false,
		startkey:[cik,year],
		endkey:[cik,year,{}]
	});
	if(rs.rows.length !== 3){
		if(rs.rows.length<3){
			console.error("Insufficient periods for EOY conversion");
		}
		else{
			// keep the document in our list of documents for
			// process, but remove the actual file. This will
			// push it back into the existing state of "download"
			let del = await global.db.upsert(rs.rows[3].id,(old)=>{
				old.submissions[0].values = [];
				return old;
			});
			console.error("Appear to already have a 4th Quarter report ("+JSON.stringify(del)+")");
		}
		rs.rows = [];
		duration = {
			locked: Date.now()+(10*24*60*1000),
		};
	}
	else{
		console.log(['FY',duration.Revenues].join('\t'));
		console.log(['~~~',duration.Revenues/4].join('\t'));
		for(let rec in rs.rows){
			rec = rs.rows[rec];
			rec = rec.doc.submissions[0].values;
			for(let f in duration){
				if(0 <= isINCEXP.indexOf(f)){
					duration[f] -= (rec[f] || 0);
				}
			}
			console.log([rec.DocumentFiscalPeriodFocus,rec.Revenues].join('\t'));
		}
		console.log([duration.DocumentFiscalPeriodFocus,duration.Revenues].join('\t'));
		console.log([duration.DocumentFiscalPeriodFocus,duration.Revenues].join('\t').replace(/[^\t]/g,'-'));
	}

	return duration;
}


async function convert10K(){
	let skip = Math.floor(Math.random()*100);
	skip = 0;
	let rs = await db.query('filings/unproc10K',{
		include_docs:true,
		limit:100,
		skip:skip,
		reduce:false
	});
	let results = [];
	for(var rec in rs.rows){
		rec = rs.rows[rec];
		let sub = rec.doc.submissions[0];
		let cik = +sub.cikNumber;
		let year = +sub.values.DocumentFiscalYearFocus;
		let fyContext = sub.values.DocumentFiscalYearFocusContext;
		// simplest case, we already have a Q4 defined
		let context = sub.values.DocumentQ4Focus;
		let q = sub.values.durations[context];
		// handle no Q4
		if(!q){
			if(!sub.values.durations[fyContext]){
				rec.doc.submissions = [];
				global.db.put(rec.doc);
				return Promise.resolve([]);
			}
			// it is possible that we are lucky, and whoever put this
			// together also recorded a Q4. While unlikely, we should
			// use theirs if it exists. Make an attempt to look for it
			let end = sub.values.durations[fyContext].endDate;
			let start = end.split('T').shift().split('-');
			start = Date.UTC(... start);
			start = new Date(start);
			start.setUTCDate(1);
			start.setUTCMonth(start.getUTCMonth()-3);
			start = start.toISOString().split('T').shift();
			context = Object.entries(sub.values.durations).filter(d=>{
				let ismatch = (d[1].startDate === start) && (d[1].endDate === end);
				return ismatch;
			});
			// grab the key of the first record... or null
			context = (context[0] || [])[0] || null;
			// grab the duration that we found
			q = sub.values.durations[context];
		}

		if(!q){
			// So we tried to get one we already created, and we tried
			// finding one that was created, but we didn't know about before.
			// Now we are going to have to create one based on the fiscal
			// year.
			let duration = sub.values.durations[fyContext];
			context = 'DocumentQ4Focus';
			q = await CreateQ4(cik,year,duration);
		}

		sub.values.durations[context] = q;
		sub.values.DocumentQ4Focus = context;
		//console.log(JSON.stringify(sub.q4,null,4));
		rec.doc = stringify(rec.doc);
		rec.doc = JSON.parse(rec.doc);
		let success = await db.put(rec.doc);
		results.push(success);
	}
	results = await Promise.all(results);
	return results;
}
(async function(){
	while(true){
		let converted = await convert10K();
		if(converted.length === 0){
			console.log(new Date().toISOString());
			console.log("waiting 10 minutes\n\n\n\n\n");
			await utils.wait(60*10*1000);
		}
	}
})();
