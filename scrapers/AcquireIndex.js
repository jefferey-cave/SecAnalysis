import * as global from './global.js';

import AcquireIndex from './lib/AcquireIndex.js';
import * as deposit from './lib/deposit.js';

async function main(){
	let factfiller = new AcquireIndex(global.sec);
	let buff = [];
	let stm = factfiller.fetch();
	for await (let company of stm) {
		let total = deposit.ReceiveData(company,global.settings)
			.then(d=>{
				total.isResolved = true;
				return d;
			})
			.catch((e)=>{
				console.error(e.message);
				global.progress.log('ERROR: '+e.message);
				total.isResolved = true;
				debugger;
				return null;
			})
			.then(d=>{
				return d;
			});

		buff.push(total);
		// setting the parallelism too high causes a TLS error
		while(buff.length >= 3){
			await Promise.race(buff);
			for(let i = 0; i< buff.length; i++){
				if(buff[i].isResolved){
					buff[i] = buff.at(-1);
					buff.pop();
					i--;
				}
			}
		}
		deposit.WriteSymbols(factfiller.symbols);

		if(global.halt){
			break;
		}
	}
	await Promise.all(buff);
	global.progress.log('Finished all companies.');
	await deposit.WriteSymbols(factfiller.symbols, true);
	global.progress.log('Finished summary secondary indexes');
	process.exit();
}

main();
