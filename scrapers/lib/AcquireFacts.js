import fs from 'node:fs';
import path from 'node:path';
import http from 'node:https';
import StreamZip from 'node-stream-zip';
import Stream from 'node:stream';
import process from 'node:process';

import {progress} from '../global.js';
import * as utils from './utils.js';
import {
	S3Client,
	ListBucketsCommand,
	ListObjectsV2Command,
	GetObjectCommand,
	PutObjectCommand,
	DeleteObjectCommand,
	//ObjectMetadata
} from "@aws-sdk/client-s3";




export {
	AcquireFacts as default,
	AcquireFacts
};


const permittedForms = [
	'10-K','10-K/A','10-KT',
	'10-Q','10-Q/A','10-QT',
	'40-F','40-F/A',
	'6-K',
	'8-K','8-K/A',
	'20-F','20-F/A',
];


const permittedFacts = [
//	'EntityCommonStockSharesOutstanding',
//	'EntityPublicFloat',
//	'AccountsPayableTradeCurrent',
//	'AccountsReceivableNetCurrent',
//	'AccruedIncomeTaxesCurrent',
];


class AcquireFacts{

	constructor(db, opts = {}){
		fs.mkdirSync(path.resolve(path.join('.', 'cache')),{ recursive: true })
		this._ = {
			opts: {
				url:'https://www.sec.gov/Archives/edgar/daily-index/xbrl/companyfacts.zip',
				cache: fs.mkdtempSync(path.resolve(path.join('.', 'cache', 'facts-')),{ recursive: true }),
				sampler: {},
				throttle:{
					recs:1357,
					freq:225,
				},
			}
		};
		this._ = Object.assign(this._, opts);

		this.db = db;

		this.entryCount = 0;
		this.entriesCount = 0;
		this.retained = 0;

	}

	get url(){
		let path = this._.opts.url;
		return path;
	}
	set url(value){
		this._.opts.url = value;
	}


 	async * fetch(){

		let zPath = new Promise((resolve)=>{
			let testcopy = path.resolve(path.join(this._.opts.cache,'..','companyfacts.zip'));
			let fpath = path.join( this._.opts.cache, 'companyfacts.zip');
			if(fs.existsSync(testcopy)){
				fs.copyFileSync(testcopy,fpath);
				resolve(fpath);
				return;
			}
			let opts = {
				headers:{
					'User-Agent': 'Individual jefferey.cave@gmail.com'
				},
			}
			let req = http.request(this.url,opts,(resp)=>{
				let stm = fs.createWriteStream(fpath);
				resp.on('data',(chunk)=>{
					stm.write(chunk);
				});
				resp.on('end',()=>{
					stm.close();
					resolve(fpath);
				});
			});
			req.end();
		});


		let allowedCompanies = this.db.send(new GetObjectCommand({Bucket: 'facts', Key: 'views/symbols'}));
		[zPath,allowedCompanies] = await Promise.all([zPath,allowedCompanies]);

		let buff = '';
		for await (let chunk of allowedCompanies.Body){
			buff += chunk.toString('utf8');
		}
		allowedCompanies = JSON.parse(buff);
		allowedCompanies = Object.values(allowedCompanies).map(d=>{return d.cik});
		//allowedCompanies = allowedCompanies.filter(d=>d===1075124);
		//console.warn(`Company list hard constrained to single company: ${allowedCompanies}`)

		console.log('Downloaded: ' + zPath);
		console.log('Permitted Companies: ' + allowedCompanies.length);



		async function ReadZipEntry(entry) {
			try{
				entry = entry.toString('utf8')
				entry = await zip.entryData(entry);
				entry = entry.toString('utf8');
				entry = JSON.parse(entry);
				return entry;
			}
			catch(e){
				console.error('Entries');
				console.error(e);
			}
		}

		async function TransformCompany(company){
			try{
				let cik = Number(company.cik);
				if(!allowedCompanies.includes(cik)){
					//console.debug('Not permitted symbol: '+cik);
					return;
				}
				else{
					//console.debug('Permitted symbol: '+cik);
				}
				let json = {
					"type": "company",
					"key": cik,
					"data": []
				};
				for(let group of Object.values(company.facts)){
					for(let fact in group){
						if(permittedFacts.length > 0 && !permittedFacts.includes(fact)){
							continue;
						}
						let units = ['USD'];
						if(!('USD' in group[fact].units)){
							units = Object.keys(group[fact].units);
						}
						for(let unit of units){
							for(let val of group[fact].units[unit]){
								if(!permittedForms.includes(val.form)){
									continue;
								}
								let rec = {
									"date":"2022-09-20",
									"unit":"text",
									"fact":"symbol",
									"val":"L"
								};
								rec.cik = cik;
								rec.fact = fact;
								rec.unit = unit;

								rec.fy = val.fy; //|| Number(val.frame.substr(2,4));
								rec.date = val.end;
								rec.val = val.val;

								json.data.push(rec);

							}
						}
					}
				}
				if(json.data.length === 0){
					json = null;
				}
				return(json);
			}
			catch(e){
				if(e.message.startsWith('Skipping')){
					//console.log(e.message);
				}
				else{
					console.error('Facts');
					console.error(e);
					//debugger;
				}
			}
			return null;
		}


		function shuffle(list){
			//return ['CIK0000001750.json'];
			//return ['CIK0001937993.json'];
			//return list.filter(d=>{return 0 <= d.localeCompare('CIK0001937992.json');}).sort().reverse();
			//return list.sort().reverse();

			let rnd, swap;
			for(let i = list.length-1; i >= 0; i--){
				rnd = Math.floor(Math.random() * i);
				swap = list[rnd];
				list[rnd] = list[i];
				list[i] = swap;
			}
			return list;
		}


		//let allowed = allowedCompanies.map(d=>`CIK${d.toString(10).padStart(10,'0')}.json`);
		let zip = new StreamZip.async({ file: zPath });
		let zipList = await zip.entries();
		zipList = Object.values(zipList);
		//console.log(`Entries available: ${this.entriesCount}`);
		zipList = zipList.filter((d)=>{return !d.isDirectory})
		zipList = zipList.map(d=>d.name);
		zipList = new Set(zipList).values();
		zipList = Array.from(zipList);
		zipList = zipList.filter(d=>{
			d = Number.parseInt(d.substr(3));
			d = allowedCompanies.includes(d);
			return d;
		});
		// because this is a large set, we shuffle the deck so that we
		// spend time picking up differnet items, not always the same ones.
		zipList = shuffle(zipList);
		this.entriesCount = zipList.length;
		this.entryCount = 0;

		let prog = progress.create(this.entriesCount,0,{label:'AcquireFact'});
		for(let entry of zipList){
			this.entryCount++;
			prog.update(this.entryCount,{label:`AcquireFact (${this.retained})`});
			entry = await ReadZipEntry(entry);
			entry = await TransformCompany(entry);
			if(!entry) continue;
			this.retained++;
			entry.source = this.constructor.name,
			yield entry;
		}
		zip.close();
		fs.rm(this._.opts.cache,{recursive:true},()=>{});
		prog.update(this.entryCount,{label:`AcquireFact (${this.retained})`});
		prog.stop();
		//console.log('Finished.')
 	}


}
