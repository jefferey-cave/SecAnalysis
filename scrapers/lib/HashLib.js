import hasher from 'hash-index';
import * as utils from './utils.js';

const MAX_KEYVAL = Math.pow(2, 8*BigUint64Array.BYTES_PER_ELEMENT)-1;


export default class HashLib{
	constructor(db,sets=[]){
		this.db = db;
		this.sets = new Map();
		for(let set of sets){
			this.load(set);
		}
	}

	async get(setName, value){
		if(!setName || !value){
			throw new Error('Parameters both required');
		}
		let s = this.sets.get(setName);
		if(!s){
			s = await this.load(setName);
		}
		let hash = s[value];
		if(!hash){
			hash = HashLib.hasher(value);
			s[value] = hash;
		}
		return hash;
	}

	async load(setName){
		let set = await this.getRemote(setName);
		let curr = {};
		if(this.sets.has(setName)){
			curr = this.sets.get(setName);
			//curr = curr.value;
		}
		set = Object.assign(set,curr);
		this.sets.set(setName,set);
		return set;
	}

	async getRemote(setName){
		let set = null;
		try{
			let key = `labels/${setName}`;
			set = await this.db.get(key);
		}
		catch(e){
			if(e.status === 404){
				set = {_id:key};
			}
			else{
				throw e;
			}
		}
		return set;
	}

	async save(){
		let saves = [];
		for(let key of this.sets.keys()){
			let set = this.sets.get(key);
			set = JSON.parse(JSON.stringify(set));
			let orig = await this.getRemote(key);
			let unique = utils.UniqueRec(orig,set);
			if(unique){
				saves.push(this.db.put(unique))
			}
		}
		saves = await Promise.all(saves);
		return saves;
	}
	
}

HashLib.hasher = function(value){
	return hasher(value,MAX_KEYVAL);
};
