
function CastBigInt(val){
    if(typeof val === 'number'){
        val = Math.round(val);
    }
    val = BigInt(val);
    return val;
}

function stats(){
    let args = Array.from(arguments);
    let rtn = BigMath.round(args.pop());
    rtn = {min: rtn,max: rtn};

    while(args.length > 0){
        let candidate = args.pop();
        candidate = BigMath.round(candidate);

        if(rtn.min > candidate){
            rtn.min = candidate;
        }
        if(rtn.max < candidate){
            rtn.max = candidate;
        }
    }
    return rtn;
}

export default class BigMath{

    static abs(val){
        val = CastBigInt(val);
        if(val < 0){
            val *= -1;
        }
        return val;
    }

    static round(val){
        val = CastBigInt(val);
        return val;
    }

    static floor(val){
        if(typeof val === 'bigint') return val;
        val = Math.floor(val);
        val = BigInt(val);
        return val;
    }

    static ceil(val){
        if(typeof val === 'bigint') return val;
        val = Math.ceil(val);
        val = BigInt(val);
        return val;
    }

    static min(){
        return stats(...arguments).min;
    }

    static max(){
        return stats(...arguments).max;
    }

}
