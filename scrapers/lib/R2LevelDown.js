
import {AbstractLevelDOWN} from 'abstract-leveldown';
import {AbstractIterator} from 'abstract-leveldown';
import fetch from 'node-fetch';
import ltgt from 'ltgt';
import xml2js from 'xml2js';
import { URL } from 'node:url';
import crypto from 'node:crypto';
import v4 from 'aws-signature-v4';
import Minio from 'minio';

import { S3Client } from "@aws-sdk/client-s3";
// Set the AWS Region.
const REGION = "auto"; //e.g. "us-east-1"
// Create an Amazon S3 service client object.
const s3Client = new S3Client({ region: REGION });

export{
	R2LevelDOWN as default
}

function lt(value) {
  return ltgt.compare(value, this._finish) < 0
}

function lte(value) {
  return ltgt.compare(value, this._finish) <= 0
}

function getStartAfterKey(key) {
  var keyMinusOneNum = (key.charCodeAt(key.length - 1) - 1)
  var keyMinusOne = keyMinusOneNum >= 0 ? (String.fromCharCode(keyMinusOneNum) + '\uFFFF') : ''
  return key.substring(0, key.length - 1) + keyMinusOne
}

function nullEmptyUndefined(v) {
  return typeof v === 'undefined' || v === null || v === ''
}

class R2Iterator extends AbstractIterator{
	constructor(db, options) {
  		super(this, db);

		this._options = options;
		this._limit   = options.limit
		if (this._limit === -1) {
			this._limit = Infinity;
		}

		this.keyAsBuffer = options.keyAsBuffer !== false;
		this.valueAsBuffer = options.valueAsBuffer !== false;
		this.fetchValues = options.values;
		this._reverse   = options.reverse;
		this._done = 0;
		this.bucket = db.bucket;
		this.db = db;
		this.r2ListObjectMaxKeys = options.r2ListObjectMaxKeys || 1000;
		if (!this._reverse && this._limit < this.r2ListObjectMaxKeys) {
			this.r2ListObjectMaxKeys = this._limit;
		}

		this._start = ltgt.lowerBound(options);
		this._finish = ltgt.upperBound(options);
		if (!nullEmptyUndefined(this._finish)) {
			if (ltgt.upperBoundInclusive(options)){
				this._test = lte;
			}
			else{
				this._test = lt;
			}
		}

		if (!nullEmptyUndefined(this._start)){
			this.startAfter = ltgt.lowerBoundInclusive(options) ? getStartAfterKey(this._start) : this._start
		}
		debug('new iterator %o', this._options);
	}


	_next(callback) {
		if (this._done++ >= this._limit || 
		(this.data && this.dataUpto == this.data.length && !this.r2nextContinuationToken))
		return setImmediate(callback)

		if (!this.data || this.dataUpto == this.data.length) {
			listObjects()
		} 
		else {
			fireCallback()
		}

		function listObjects() {
		var params = {
			Bucket: this.bucket,
			MaxKeys: this.r2ListObjectMaxKeys
		}

		if (this.db.folderPrefix !== '') {
			params.Prefix = this.db.folderPrefix
		}

		if (this.r2nextContinuationToken) {
			params.ContinuationToken = this.r2nextContinuationToken
			debug('listObjectsV2 ContinuationToken %s', params.ContinuationToken)
		}
		else if (typeof this.startAfter !== 'undefined') {
			params.StartAfter = this.db.folderPrefix + this.startAfter
		}

		this.db.listObjectsV2(params, function(err, data) {
			if (err) {
				debug('listObjectsV2 error %s', err.message)
				callback(err)
			} else {
			if (data.Contents.length === 0) {
				debug('listObjectsV2 empty')
				return setImmediate(callback)
			}

			debug('listObjectsV2 %d keys', data.Contents.length)

			if (this.data && this.dataUpto === 0) {
				this.data = this.data.concat(data.Contents)
			} else {
				this.data = data.Contents
			}

			this.dataUpto = 0
			this.r2nextContinuationToken = data.NextContinuationToken

			if (this._reverse && this.r2nextContinuationToken &&
				data.Contents.every(function(x) {
				return this._test(x.Key.substring(this.db.folderPrefix.length, x.Key.length)) })
				) {
				listObjects()
			} else {
				fireCallback()
			}
			}
		})
		}


		function fireCallback() {
		var index, key
		for(;;) {
			index = (!this._reverse) ? this.dataUpto : (this.data.length - 1 - this.dataUpto)
			var awskey = this.data[index].Key
			key = awskey.substring(this.db.folderPrefix.length, awskey.length)
			debug('iterator data index %d: %s', index, key)
			this.dataUpto++

			if (this._test(key)) {
			break
			}

			if (!this._reverse || this.dataUpto === this.data.length) {
			return setImmediate(callback)
			}
		}

		if (this.fetchValues) {
			if (this.data[index].Size === 0){
				getCallback(null, '')
			}
			else{
				this.db._get(key, null, getCallback)
			}
		}
		else{
			getCallback();
		}

		function getCallback(err, value) {
			if (err) {
			if (err.message == 'NotFound') {
				// collection changed while we were iterating, skip this key
				return setImmediate(function () {
				this._next(callback)
				})
			}
			return setImmediate(function () {
				callback(err)
			})
			}

			if (this.keyAsBuffer && !(key instanceof Buffer))
			key = new Buffer(key)
			if (!this.keyAsBuffer && (value instanceof Buffer))
			key = key.toString('utf8')

			if (this.fetchValues) {
			if (this.valueAsBuffer && !(value instanceof Buffer))
				value = new Buffer(value)
			if (!this.valueAsBuffer && (value instanceof Buffer))
				value = value.toString('utf8')
			}

			setImmediate(function () {
			debug('_next result %s=%s', key, value)
			callback(null, key, value)
			})
		}
		}
	}

	_test() { 
		return true;
	}
}

class R2LevelDOWN extends AbstractLevelDOWN{

	constructor(options) {
		super();

		this.options = options || {};

		this.AcctId = options.AcctId;
		this.AccessKey = options.AccessKey;
		this.SecretKey = options.SecretKey;
		this.Bucket = options.Bucket;

		this.Timeout = options.Timeout || 10000;

		this.endpoint = `https://${this.AcctId}.r2.cloudflarestorage.com/${this.Bucket}`;

	}

	_create(){
		fetch(this.endpoint, {
			method: 'PUT',
			headers: this.makeHeaders()
		})
	}

	/**
	 * https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html
	 * 
	 * @param {*} method 
	 * @param {*} url 
	 * @param {*} body 
	 * @returns 
	 */
	fetch(method='GET',url='/',body){
		let region = 'auto';
		let AccessKey = this.AccessKey;
		let SecretKey = this.SecretKey;

		method = method.toLocaleUpperCase();
		url = new URL(`${url}`,this.endpoint);

		let date = new Date();
		date = date.toISOString().replace(/[-\.:]/g,'');

		// test values from 
		// https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html#example-signature-GET-object
		/*
		method = 'GET';
		url = new URL('http://examplebucket.s3.amazonaws.com/test.txt');
		date = '20130524T000000Z';
		body = '';
		region = 'us-east-1';
		AccessKey = 'AKIAIOSFODNN7EXAMPLE';
		SecretKey ='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY';
		*/

		// test values from 
		// https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html#example-signature-GET-bucket-lifecycle
		/*
		method='GET';
		url = new URL('http://examplebucket.s3.amazonaws.com/?lifecycle');
		date = '20130524T000000Z';
		region = 'us-east-1';
		AccessKey = 'AKIAIOSFODNN7EXAMPLE';
		SecretKey ='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY';
		*/

		// test values from 
		// https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html#example-signature-GET-bucket-lifecycle
		/*
		method='GET';
		url = new URL('http://examplebucket.s3.amazonaws.com/?max-keys=2&prefix=J');
		date = '20130524T000000Z';
		region = 'us-east-1';
		AccessKey = 'AKIAIOSFODNN7EXAMPLE';
		SecretKey ='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY';
		*/

		// test values from 
		// https://web.postman.co/workspace/My-Workspace~34f9f0c2-7560-43f1-87af-554d9b320eb9/request/161207-790427f2-7c2b-499b-944f-1d05cb29d09a?ctx=code
		method = 'GET';
		date = '20220824T214955Z';
		body = '';
		region = 'auto';

		let contentHash = crypto.createHash('sha256').update(body).digest('hex');

		let headers = {
			'Host': url.host,
			//'Content-Type': 'application/json',
			'X-Amz-Content-Sha256': contentHash,
			'X-Amz-Date': date
		};

		let canonicalHeaders = Object.entries(headers)
			.sort((a,b)=>{return a[0].localeCompare(b[0]); })
			.map(d=>{
				let result =  [
					d[0].toLocaleLowerCase().trim(),
					d[1].trim()
				];
				result = result.join(':');
				return result;
			})
			.join('\n')+'\n'
			;

		let signedHeaders = Object.keys(headers).sort().join(';').toLowerCase();
		

		let canonicalRequest = [
			method,
			url.pathname,
			Array.from(url.searchParams.entries()).map((d)=>{return `${d[0]}=${d[1]}`;}).join('&'),
			canonicalHeaders,
			signedHeaders,
			contentHash
		].join('\n');

		let scope = [date.split('T').shift(),region,'s3','aws4_request'].join('/');

		let sigstring = [
			"AWS4-HMAC-SHA256",
			date,
			scope,
			crypto.createHash('sha256').update(canonicalRequest).digest('hex')
		].join('\n');

		let sig = crypto.createHmac('sha256',"AWS4"+SecretKey).update(date.split('T').shift()).digest();
		sig = crypto.createHmac('sha256',sig).update(region).digest();
		sig = crypto.createHmac('sha256',sig).update('s3').digest();
		sig = crypto.createHmac('sha256',sig).update('aws4_request').digest();
		sig = crypto.createHmac('sha256',sig).update(sigstring).digest('hex');

		headers.Authorization = `AWS4-HMAC-SHA256 Credential=${AccessKey}/${scope},SignedHeaders=${signedHeaders},Signature=${sig}`;

		let f = fetch(url, {
			method: 'GET', 
			headers: headers,
		});
		return f;
	}

	_open(options, callback) {
		let url = new URL(this.endpoint);
		this.client = new Minio.Client({
			endPoint: url.host,
			port: url.port,
			region: 'auto',
			useSSL: true,
			accessKey: this.AccessKey,
			secretKey: this.SecretKey
		});
		this.client.bucketExists(this.Bucket, function(err, exists) {
			setTimeout(()=>{
				if(err){
					callback(err,exists);
				}
				else if(!exists){
					callback(exists);
				}
				else{
					callback();
				}
			});
		});
	}

	_put(key, value, options, callback) {
		if (nullEmptyUndefined(value)){
			value = Buffer.from('');
		}
		if (!(value instanceof Buffer || value instanceof String)){
			value = String(value);
		}

		let metadata = {};
		this.client.putObject(this.Bucket, key, value, metadata, function(err, etag) {
			if (err) {
				//console.debug('Error r2 upload: %s %s', key, err.message);
				callback(err);
			} 
			else {
				//console.debug('Successful r2 upload: %s', key);
				callback();
			}
		});
	}


	_get(key, options, callback) {
		let timeout = setTimeout(()=>{
			callback(new Error('Timed out getting ' + key))
		},this.Timeout)
		this.client.getObject(this.Bucket, key, {}, function (err, data) {
			clearTimeout(timeout);
			if (err) {
				//console.debug('Error r2 getObject: %s %s', key, err.message);
				if (err.code === 'NoSuchKey') {
					callback(new Error('NotFound'));
				}
			} 
			else {

				let buf = [];
				data.on("error", (err) => callback(err));
				data.on("data", (chunk) => buf.push(chunk));
				data.on("end", () => {
					let value = Buffer.concat(buf);
					if (options && options.asBuffer && !(value instanceof Buffer)){
						value = Buffer.from(value);
					}
					if ((!options || !options.asBuffer) && (value instanceof Buffer)){
						value = value.toString('utf8');
					}
					//console.debug('getObject: %s', key);
					callback(null,value);
				});
			}
		});
	}


	_del(key, options, callback) {
		let timeout = setTimeout(()=>{
			callback(new Error('Timed out deleting ' + key))
		},this.Timeout)
		if(!Array.isArray(key)){
			key = [key];
		}
		if(key.length<=0){
			setTimeout(callback);
			return;
		}
		this.client.removeObjects(this.Bucket,key, (err)=>{
			clearTimeout(timeout);
			if (err) {
				console.debug('Error r2 delete: %s %s', key, err.message);
			} 
			else {
				console.debug('Successful r2 delete: %s', key);
			}
			callback(err);
		});
	}


	_batch(array, options, done) {
		let buffer = [];
		let dels = array
			.filter(d=>{
				return d.type === 'del';
			})
			.map(d=>{
				return d.key;
			})
			;
		if(dels.length > 0) buffer.push(new Promise((resolve,reject)=>{
			this._del(dels,{},function(err){
				if(err){
					reject(err);
				}
				else{
					resolve(dels);
				}
			});	
		}));
		buffer = buffer.concat(array
			.filter(d=>{
				return d.type === 'put';
			})
			.map(d=>{
				return new Promise((resolve,reject)=>{
					this._put(d.key,d.value,{},(err)=>{
						if(err){
							reject(err);
						}
						else{
							resolve(d.key);
						}
					});
				});
			})
		);
		Promise
			.all(buffer)
			.then((e)=>{
				done();
			})
			.catch((e)=>{
				done(e);
			})
			;
	}

	_getMany(keys, options, callback){
		let timeout = setTimeout(()=>{
			if(callback) callback(new Error('Timed out'))
		},this.Timeout)
		let gets = keys.map((key)=>{
			return new Promise((resolve,reject)=>{
				this.get(key,options,(err,value)=>{
					if(err){
						if(err.message === 'NotFound'){
							resolve(undefined);
						}
						else{
							reject(err);
						}
					}
					else{
						resolve(value);
					}
				});
			});
		});
		Promise
			.all(gets)
			.then((data)=>{
				callback(null,data);
				callback = null;
			})
			.catch((e)=>{
				callback(e)
				callback = null;
			})
			.finally(()=>{
				clearTimeout(timeout);
			})
			;
	}

	async _clear(){
		return new Promise((resolve,reject)=>{
			try{
				let dels = [];
				let buf = [];
				let objectsStream = this.client.listObjects('facts', '', true);
				objectsStream
					.on('data', (obj)=>{
						buf.push(obj.name);
						if(buf.length > 10){
							dels.push(new Promise(r=>{
								this._del(buf,{}, function(e) {
									if (e) {
										return console.log('Unable to remove Objects ',e)
									}
									console.log('Removed the objects successfully')
									r();
								})
							}));
							buf = [];
						}
					})
					.on('error', function(e) {
						console.log(e);
					})
					.on('end',()=>{
						dels.push(new Promise(r=>{
							this._del(buf,{},r);
						}));
						setTimeout(()=>{
							resolve(Promise.all(dels));
						});
					})
					;
			}
			catch(e){
				setTimeout(()=>{
					reject(e);
				});
				debugger;
			}
		});
	}

	_iterator(options) {
		return new R2Iterator(this, options)
	}
}
