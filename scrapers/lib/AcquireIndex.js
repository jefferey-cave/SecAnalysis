import fs from 'node:fs';
import path from 'node:path';
import http from 'node:https';
import Hasher from './HashLib.js';
import StreamZip from 'node-stream-zip';

import JSONBigint from 'json-bigint';
const JSONBig = JSONBigint({strict:true, useNativeBigInt:true});

import {progress} from '../global.js';
import * as utils from './utils.js';

export {
	AcquireIndex as default,
	AcquireIndex,
}

const MAX_KEYVAL = Math.pow(2, 8*BigUint64Array.BYTES_PER_ELEMENT)-1;

let self = null;

class AcquireIndex{

	constructor(db, opts = {}){
		fs.mkdirSync(path.resolve(path.join('.', 'cache')),{ recursive: true })
		this._ = {
			opts: {
				url:'https://www.sec.gov/Archives/edgar/daily-index/bulkdata/submissions.zip',
				cache: fs.mkdtempSync(path.resolve(path.join('.', 'cache', 'facts-')),{ recursive: true }),
				sampler: {
					//cik: 1,
				},
				throttle:{
					recs:1357,
					freq:250,
				},
			}
		};
		this.db = db;
		this.incompleteFiles = {};
		this.emitbuffer = new Map();
		this.hashbuffer = new Hasher(db);
		this.nextPush = Date.now();

		this.symbols = {};
		this.entryCount = 0;
		this.entriesCount = 0;
		this.retained = 0;

		self = this;
	}

	async * fetch(){
		//await this.db._db._clear();return;

		let zPath = await new Promise((resolve)=>{
			let testcopy = path.resolve(path.join(this._.opts.cache,'..','submissions.zip'));
			let fpath = path.join( this._.opts.cache, 'submissions.zip');
			if(fs.existsSync(testcopy)){
				fs.copyFileSync(testcopy,fpath);
				resolve(fpath);
				return fpath;
			}
			let opts = {
				headers:{
					'User-Agent': 'Individual jefferey.cave@gmail.com'
				},
			}
			let req = http.request(this._.opts.url,opts,(resp)=>{
				let stm = fs.createWriteStream(fpath);
				resp.on('data',(chunk)=>{
					stm.write(chunk);
				});
				resp.on('end',()=>{
					stm.close();
					resolve(fpath);
				});
			});
			req.end();
		});


		async function ByteToJson(entry) {
			try{
				entry = entry.toString('utf8')
				entry = await zip.entryData(entry);
				entry = entry.toString('utf8');
				if(entry === 'Placeholder file'){
					return {};
				}
				entry = JSONBig.parse(entry);
				return entry;
			}
			catch(e){
				console.error('Entries');
				console.error(e);
			}
		}




		await utils.wait(500);
		let zip = new StreamZip.async({ file: zPath });
		await utils.wait(500);
		let zipList = await zip.entries();
		zipList = Object.values(zipList);
		zipList = zipList.filter((d)=>{return !d.isDirectory})
		zipList = zipList.map(d=>d.name);
		zipList = zipList.filter((d)=>{return d !== 'Placeholder file'});
		zipList = shuffle(zipList);
		this.entriesCount = zipList.length;

		let prog = progress.create(this.entriesCount,0,{label:'AcquireIndex'});

		for(let zipEntry of zipList){
			this.entryCount++;
			prog.update(this.entryCount,{label:'AcquireIndex'});
			let rtn = new Promise(async (resolve)=>{
				let stage = '';
				let timer = setTimeout(()=>{
					console.warn(`${zipEntry}: skipped due to time out (stage: ${stage})`);
					resolve(null);
				},60000);
				stage = 'ByteToJson';
				let entry = await ByteToJson(zipEntry);
				stage = 'RecordFilter';
				entry = await RecordFilter(entry);
				if (entry) {
					this.retained++;
					stage = 'TransformCompany';
					entry = await TransformCompany(entry);
					entry.source = this.constructor.name;
				}
				clearTimeout(timer);
				stage = 'Returned';
				resolve(entry);
				return;
			});
			rtn = await rtn;
			if (rtn) yield rtn;
		}

		prog.stop();
		// Don't remove this progress bar. We want it to stick around as it represents the whole run
		//progress.remove(prog);

		zip.close();
		fs.rm(this._.opts.cache,{recursive:true},()=>{});
		//console.log("DONE!");

		return;
	}


	static transformitem(item){
		if(!('cik' in item)) return null;

		/*
		 * These values are a list of dates that filings occured on. We can assume
		 * that the biographic data is recent as of the most recent filing.
		 */
		{
			let asats = [
				'reportDate',
				'filingDate',
				'acceptanceDateTime',
			]
			for(let a of asats){
				if(a in item.filings.recent){
					item[a] = item.filings.recent[a].shift();
				}
			}
		}

		/*
		 * Some of the fields are better represented by different datatypes
		 * than they are sent in. Convert them.
		 */
		{
			// Numerics
			let numerics = [
				'cik',
				'sic',
			];
			for(let n of numerics){
				if(n in item){
					item[n] = +item[n];
				}
			}
			// Booleans
			let booleans = [
				'insiderTransactionForIssuerExists',
				'insiderTransactionForOwnerExists',
			];
			for(let b of booleans){
				if(b in item){
					item[b] = !!item[b];
				}
			}
		}

		/*
		 * fiscal year end is given as a month/day for all records. We
		 * know what year this is particular report is for, so we can
		 * put a year on the fiscal year end.
		 */
		{
			//if(item.cik === 10012 && item.reportDate.split('-').shift() < 1996){
			//	debugger;
			//}
			// FY has been null, choose dec 31st arbitrarily
			item.fiscalYearEnd = item.fiscalYearEnd || '1231';
			// Parse the date components
			let year = null;
			try{
				year = +(item.reportDate || item.filingDate || '1900').split('-').shift();
			}
			catch(e){
				console.error(e);
				debugger;
			}
			let month = +item.fiscalYearEnd.slice(0,2)-1;
			let day = +item.fiscalYearEnd.slice(2,4);
			// put them together
			let fy = Date.UTC(year,month,day);
			fy = new Date(fy);
			// determine the fiscal year and fiscal year end
			item.year = fy.getUTCFullYear();
			if(fy < new Date(Date.UTC(item.reportDate || fy))){
				item.year++;
				fy.setUTCFullYear(item.year+1);
			}
			item.fiscalYearEnd = fy.toISOString().split('T').shift();
		}


		/*
		 * tickers and exchanges are disparate, that's dumb
		 */
		if('tickers' in item && item.tickers.length){
			item.symbols = [];
			while(item.tickers.length){
				item.symbols.push([
					item.exchanges.pop(),
					item.tickers.pop()
				]);
			}
		}


		/*
		 * Some objects are obviously collections
		 */
		item.sic = {
			'code': item.sic,
			'label': item.sicDescription,
		};
		delete item.sicDescription;

		if(item.primaryDocument || item.primaryDocDescription){
			item.primaryDoc = {
				'name': item.primaryDocument,
				'desc': item.primaryDocDescription
			};
			delete item.primaryDocument;
			delete item.primaryDocDescription;
		}

		item.stateOfIncorporation = {
			'code': item.stateOfIncorporation,
			'desc': item.stateOfIncorporationDescription,
		};
		delete item.stateOfIncorporationDescription;

		/*
		 * A couple of very simple updates
		 */
		if(item.acceptanceDateTime){
			item.acceptanceDateTime = item.acceptanceDateTime.split('.').shift();
		}
		delete item.filings;

		/*
		 * remove empty records
		 */
		for(let key of Object.keys(item)){
			let remove = false;
			remove = remove || item[key] === '';
			remove = remove || item[key] === null;
			remove = remove || typeof item[key] === 'undefined';
			remove = remove || (Array.isArray(item[key]) && !item[key].length);
			if(remove){
				delete item[key];
			}
		}

		/*
		 * Done
		 */
		return item;
	}




}


async function RecordFilter(entry){
	try{
		//if(debugmode){
		//	debugger; //cik: 1947264
		//}
		let company = AcquireIndex.transformitem(entry);
		if(!company || !company.cik){
			throw new Error('Skipping: CIK not present');
		}
		if(!company.filingDate){
			throw new Error('Skipping: FilingDate not present');
		}
		if(!('symbols' in company)){
			throw new Error('Skipping:' + company.cik + ' has no stock symbols');
		}
		if('cik' in self._.opts.sampler){
			if(!utils.sampler(company.cik,self._.opts.sampler.cik)){
				throw new Error('Skipping:' + company.cik + ' got sampled out');
			}
		}
		return company;
	}
	catch(e){
		if(e.message.startsWith('Skipping')){
			//console.debug(e.message);
		}
		else{
			console.error('Filter');
			console.error(e);
		}
	}
	return null;
}

async function TransformCompany(entry){
	try{
		//if(debugmode){
		//	debugger;
		//}
		/// DO A BUNCH OF FACT PARSING HERE
		let facts = {
			'entitytype': entry.entityType,
			'sic':entry.sic.code,
			'name':entry.name,
			'category':entry.category,
			'fiscalYearEnd': entry.fiscalYearEnd,
			'stateOfIncorporation': entry.stateOfIncorporation.code,
			'symbol': entry.symbols[0][1],
			'market': entry.symbols[0][0],
		};
		// collect the symbols we have discovered and save them to the symbol pool
		// The pool is periodically written to the datastore
		self.symbols[facts.symbol] = self.symbols[facts.symbol] || {
			market: facts.market,
			symbol: facts.symbol,
			date: entry.filingDate,
			cik: entry.cik,
			asat: (new Date()).toISOString(),
		};

		let json = {
			"type": "company",
			"key": entry.cik,
			"data": []
		};
		let recTemplate = {
			cik: entry.cik,
			date: entry.filingDate,
			unit: 'text',
			fy: Number(entry.fiscalYearEnd.split('-').shift()),
			form: 'compdex',
		};
		for(let fact of Object.entries(facts)){
			if(typeof fact[1] === 'undefined') continue;
			let rec = Object.assign({},recTemplate);
			rec.fact = fact[0].toLowerCase();
			rec.val = fact[1];
			if(Number.isInteger(rec.val)){
				rec.unit = 'number';
			}
			json.data.push(rec);
		}
		return json;
	}
	catch(e){
		console.error('Factor');
		console.error(e);
		debugger;
	}
}

function shuffle(list){
	//return ['CIK0000001750.json'];
	//return ['CIK0001937993.json'];
	//return list.filter(d=>{return 0 <= d.localeCompare('CIK0001937992.json');}).sort().reverse();
	//return list.sort().reverse();

	let rnd, swap;
	for(let i = list.length-1; i >= 0; i--){
		rnd = Math.floor(Math.random() * i);
		swap = list[rnd];
		list[rnd] = list[i];
		list[i] = swap;
	}
	return list;
}